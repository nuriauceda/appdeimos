import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//clase pojo pare recoger los datos de la API REST https://cat-fact.herokuapp.com/facts
public class CatFact {

    @SerializedName("status")
    @Expose
    private Status status;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("user")
    @Expose
    private String user;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("__v")
    @Expose
    private Integer v;
    @SerializedName("source")
    @Expose
    private String source;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("deleted")
    @Expose
    private Boolean deleted;
    @SerializedName("used")
    @Expose
    private Boolean used;

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Boolean getUsed() {
        return used;
    }

    public void setUsed(Boolean used) {
        this.used = used;
    }

    @Override
    public String toString() {
        return "\nFact{" +
                "\n id: '" + id + '\'' +
                "\n text: '" + text + '\'' +
                "\n user: '" + user + '\'' +
                "\n v: " + v +
                "\n source: '" + source + '\'' +
                "\n updatedAt: '" + updatedAt + '\'' +
                "\n type: '" + type + '\'' +
                "\n createdAt: '" + createdAt + '\'' +
                "\n deleted: " + deleted +
                "\n used: " + used +
                "\n Status: " + status +
                "\n}\n" ;
    }

    private class Status {

        @SerializedName("verified")
        @Expose
        private Boolean verified;
        @SerializedName("feedback")
        @Expose
        private String feedback;
        @SerializedName("sentCount")
        @Expose
        private Integer sentCount;

        public Boolean getVerified() {
            return verified;
        }

        public void setVerified(Boolean verified) {
            this.verified = verified;
        }

        public String getFeedback() {
            return feedback;
        }

        public void setFeedback(String feedback) {
            this.feedback = feedback;
        }

        public Integer getSentCount() {
            return sentCount;
        }

        public void setSentCount(Integer sentCount) {
            this.sentCount = sentCount;
        }

        @Override
        public String toString() {
            return "" +
                    "\n\tverified: " + verified +
                    "\n\tfeedback: '" + feedback + '\'' +
                    "\n\tsentCount: " + sentCount +
                    "\n\n";
        }
    }
}