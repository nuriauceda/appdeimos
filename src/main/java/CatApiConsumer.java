import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.util.List;

public class CatApiConsumer {

    public static void retrieveData() {

        String url = "https://cat-fact.herokuapp.com";

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ICatFactEndpoint service = retrofit.create(ICatFactEndpoint.class);

        Call<List<CatFact>> repos = service.getFacts();
        repos.enqueue(new Callback<List<CatFact>>() {
            @Override
            public void onResponse(Call<List<CatFact>> call, Response<List<CatFact>> response) {
                System.out.println("\n----- facts data from " + url + " -----\n");
                System.out.println(response.body().toString());

            }

            @Override
            public void onFailure(Call<List<CatFact>> call, Throwable t) {
                System.out.println("fail to retrieve information");
            }
        });
    }
}
