import retrofit2.Call;
import retrofit2.http.GET;

import java.util.List;

public interface ICatFactEndpoint {
    @GET("/facts")
    Call<List<CatFact>> getFacts();
}
